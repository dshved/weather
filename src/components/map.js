import React, {Component} from 'react';
import { YMaps, Map, Placemark } from 'react-yandex-maps';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as cityActions from '../actions/city';





class Maps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      center: [55.4312453, 37.54576470000006],
      zoom: 10
    };
  }

  componentWillReceiveProps(nextProps) {
    switch (nextProps.currentCity.type) {
      case 'CURRENT_CITY':
        this.setState({
          center: nextProps.currentCity.item.geometry.coordinates,
        })
        return nextProps;
      default:
        return this.props;
    }
  }

  render() {
   
    return (
      <div className="map clearfix">
        <YMaps>
          <Map state={this.state} width="100%" height={400}>
            {this.props.city.map((placemarkParams, i) => (
              <Placemark key={i} {...placemarkParams} />
            ))}
          </Map>
        </YMaps>
      </div>
    )
  }
}

function mapStateToProps(state, props) {
  return {
    city: state.city,
    currentCity: state.currentCity
  };
}

function mapDispatchToProps(dispatch) {
  return {
    cityActions: bindActionCreators(cityActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Maps);
