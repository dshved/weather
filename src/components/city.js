import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as cityActions from '../actions/city';


class Cities extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.onCurrentCity = this.onCurrentCity.bind(this);
    this.onRemoveCity = this.onRemoveCity.bind(this);
  }

  onCurrentCity(item) {
    this.props.cityActions.currentCity(item);
  }

  onRemoveCity(name) {
    this.props.cityActions.removeCity(name);
  }

  render() {
    const cityList = this.props.city.map((item, idx) => {
      return (
        <li key={idx} className="city__item" >
          <div className="city__info" onClick={() => this.onCurrentCity(item)}>
            <span className="city__name" >{item.name}</span>
            <span className="city__temperature">{item.temperature}</span>
          </div>            
          <span className="city__remove" onClick={() => this.onRemoveCity(item.name)}>Удалить</span>
        </li>
      );
    });
    return (
      <div className="city clearfix">
        <ul className="city__list">
          {cityList}
        </ul>
      </div>
    )
  }
}


function mapStateToProps(state, props) {
  return {
    city: state.city,

  };
}

function mapDispatchToProps(dispatch) {
  return {
    cityActions: bindActionCreators(cityActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Cities);
