import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';

import * as cityActions from '../actions/city';

class Places extends Component {
  constructor(props) {
    super(props)
    this.state = {
      address: '',
      city: '',
      coord: {},
      loading: false
    }
    this.handleSelect = this.handleSelect.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.addCity = this.addCity.bind(this)
    this.getTemperature = this.getTemperature.bind(this)
  }

  handleSelect(address) {
    this.setState({
      address,
      loading: true
    })

    geocodeByAddress(address)
      .then((results) => {
        this.setState({
          city: results[0].address_components[0].long_name
        });
        return getLatLng(results[0])
      })
      .then(({ lat, lng }) => {
        this.setState({
          coord: {
            lat,
            lng
          },
          loading: false
        })
        this.addCity(this.state);
        this.setState({
          address: ''
        })
      })
      .catch((error) => {
        console.log('Oh no!', error)
        this.setState({
          loading: false
        })
      })

  }

  handleChange(address) {
    this.setState({
      address,
    })
  }

  getTemperature(lat, lng, cb) {
    fetch(`http://api.openweathermap.org/data/2.5/weather?units=metric&lat=${lat}&lon=${lng}&appid=3ed3f96f5a260905f069945ddff661aa`)
      .then(function(response) {
        return response.json();
       })
      .then(function(city) {
        return cb(Math.floor(city.main.temp));
      })
  }

  addCity(state) {
    this.getTemperature(state.coord.lat, state.coord.lng, (temp) => {
      const city = {
        name: state.city,
        temperature: temp,
        geometry: {
          coordinates: [
            state.coord.lat,
            state.coord.lng
          ]
        },
        properties: {
          iconCaption: temp
        },
        options: {
          preset: "islands#greenDotIconWithCaption",
          iconCaptionMaxWidth: 50
        }
      }
      this.props.cityActions.addCity(city);
      this.props.cityActions.currentCity(city);
    });

  }

  render() {
    const cssClasses = {
      root: 'search',
      input: 'search__input',
      autocompleteContainer: 'search__suggestion',
    }

    const AutocompleteItem = ({ formattedSuggestion }) => (
      <div className="suggestion-item">
        <i className='fa fa-map-marker suggestion-icon'/>
        <strong>{formattedSuggestion.mainText}</strong>{' '}
        <small className="text-muted">{formattedSuggestion.secondaryText}</small>
      </div>)

    const inputProps = {
      type: "text",
      value: this.state.address,
      onChange: this.handleChange,
      autoFocus: true,
      placeholder: "Введите город",
      name: 'Demo__input',
      id: "my-input-id",
    }

    return (
      <div className="search">
        <PlacesAutocomplete
          onSelect={this.handleSelect}
          autocompleteItem={AutocompleteItem}
          onEnterKeyDown={this.handleSelect}
          classNames={cssClasses}
          inputProps={inputProps}
          googleLogo={false}
        />
      </div>
    )
  }
}

function mapStateToProps(state, props) {
  return {
    city: state.city,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    cityActions: bindActionCreators(cityActions, dispatch)
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Places);