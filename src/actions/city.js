export const addCity = (city) => {
  return {
    type: 'ADD_CITY',
    city
  };
}

export const currentCity = (item) => {
  return {
    type: 'CURRENT_CITY',
    item
  };
}

export const removeCity = (name) => {
  return {
    type: 'REMOVE_CITY',
    name
  };
}