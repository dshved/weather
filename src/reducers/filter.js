const currentCity = (state = {}, action) => {
  switch (action.type) {
    case 'CURRENT_CITY':
      return action;
    default:
      return state;
  }
}

export default currentCity;