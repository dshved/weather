import { combineReducers } from 'redux';
import city from './city';
import currentCity from './filter';

const rootReducer = combineReducers({
  city,
  currentCity
});

export default rootReducer;