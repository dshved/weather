const city = (state = [], action) => {
  switch (action.type) {
    case 'ADD_CITY':
      return [...state, action.city];
    case 'REMOVE_CITY':
      return state.filter((item) => item.name !== action.name);
    default:
      return state;
  }
}

export default city;