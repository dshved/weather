import React, { Component } from 'react';
import './App.css';
import Places from './components/places';
import Cities from './components/city';
import Maps from './components/map';

class App extends Component {
  render() {
    return (
      <div className="wrapper clearfix">
        <div className="container">
          <Places />
          <Cities />
          <Maps />
        </div>
      </div>
    );
  }
}

export default App;
